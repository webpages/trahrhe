# trahrhe 4.0 - 14 October 2022

- Some bugs fixed
- implements a major extension with new alternatives for the runtime computation of trahrhe expressions values, that may significantly improve the time performance when using the generated C header files:
* when activated, adds a precision correction mechanism. Useful when the complexity of the trahrhe expressions yields annoying precision issues. Moreover, it makes the use of MPC useless and thus yields significantly better time performance than when using MPC.
* when activated, avoids the computation of the trahrhe expressions by solving polynomial equations, and uses instead a dichotomic search mechanism to get the current integer values of the trahrhe expression, through invocations to the ranking polynomials. Another improved dichotomic search may also be activated, that uses the history of previous invocations to accelerate even more the search.

