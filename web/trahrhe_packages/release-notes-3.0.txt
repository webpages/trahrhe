# trahrhe 3.0 - 18 August 2021

- Some bugs fixed
- Removed useless computations of trahrhe expressions for inner dimensions, when launching the software for algebraic tiling. This improvement should yield better performance and enlarged scope.
