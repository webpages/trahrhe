#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
 
/******************************** TILEMIN ********************************/
static inline long int TILEMIN(long int N) {
  long int TMIN=0,TMP;
  TMP=floorl(creall(cpowl((long double)N,(long double)2))+0.0000001);
  if (TMP > TMIN) TMIN=TMP;
 
 
  return TMIN;
} /* end TILEMIN */
 
/******************************** i Ehrhart Polynomials ********************************/
static inline long int i_Ehrhart(long int N) {
 
  if ((N>0)) {
    return floorl(creall((cpowl((long double)N,(long double)3)+cpowl((long double)N,(long double)2))/(long double)2)+0.0000001);
  }
  fprintf(stderr,"Error i_Ehrhart: no corresponding domain: (N) = (%ld)\n",N);
  exit(1);
}  /* end i_Ehrhart */
 
/******************************** i Ranking Polynomials ********************************/
static inline long int i_Ranking(long int i, long int j, long int k,long int N) {
 
  if ((i>=0) && (((0<=j) && (j<N))) && (((i<=k) && (k<N)))) {
    return floorl(creall(((long double)2*(long double)k+((long double)2*(long double)N-(long double)2*(long double)i)*(long double)j-(long double)N*cpowl((long double)i,(long double)2)+((long double)2*cpowl((long double)N,(long double)2)+(long double)N-(long double)2)*(long double)i+(long double)2)/(long double)2)+0.0000001);
  }
  fprintf(stderr,"Error i_Ranking: no corresponding domain: (i, j, k, N) = (%ld, %ld, %ld, %ld)\n",i, j, k,N);
  exit(1);
} /* end i_Ranking */
 
/******************************** i_PCMin ********************************/
/******************************** i_PCMin_1 ********************************/
static inline long int i_PCMin_1(long int N) {
 
  if ((N>=1)) {
    return floorl(creall((long double)1)+0.0000001);
  }
  return i_Ehrhart(N);
} /* end i_PCMin_1 */
 
/******************************** i_PCMax ********************************/
/******************************** i_PCMax_1 ********************************/
static inline long int i_PCMax_1(long int N) {
 
  if ((N>=1)) {
    return floorl(creall((cpowl((long double)N,(long double)3)+cpowl((long double)N,(long double)2))/(long double)2)+0.0000001);
  }
  return 0;
} /* end i_PCMax_1 */
 
/******************************** i_trahrhe_i ********************************/
static inline long int i_trahrhe_i(long int pc, long int N) {
 
  if ( (i_PCMin_1(N) <= pc) && (pc <= i_PCMax_1(N)) ) {
 
  long int i = floorl(creall(-(csqrtl(-(long double)N*((long double)8*(long double)pc-(long double)4*cpowl((long double)N,(long double)3)-(long double)4*cpowl((long double)N,(long double)2)-(long double)N-(long double)8))-(long double)2*cpowl((long double)N,(long double)2)-(long double)N)/((long double)2*(long double)N))+0.0000001);
  if ((N>=i+1) && ((i>=0))) {
    return i;
  }
  }
 
  fprintf(stderr,"Error i_trahrhe_i: no corresponding domain: (pc, N) = (%ld,%ld)\n",pc,N);
  exit(1);
} /* end i_trahrhe_i */
 
/******************************** i_trahrhe_j ********************************/
static inline long int i_trahrhe_j(long int pc, long int i, long int N) {
 
  if ( ((N>=i+1) && ((i>=0))) && (i_PCMin_1(N) <= pc) && (pc <= i_PCMax_1(N)) ) {
 
  long int j = floorl(creall(-((long double)2*(long double)pc+(long double)N*cpowl((long double)i,(long double)2)-(long double)2*cpowl((long double)N,(long double)2)*(long double)i-(long double)N*(long double)i-(long double)2)/((long double)2*((long double)i-(long double)N)))+0.0000001);
  if ((N>=i+1) && ((i>=0)) && ((N>=j+1)) && ((j>=0))) {
    return j;
  }
  }
 
  fprintf(stderr,"Error i_trahrhe_j: no corresponding domain: (pc, i, N) = (%ld,%ld, %ld)\n",pc,i,N);
  exit(1);
} /* end i_trahrhe_j */
 
/******************************** i_trahrhe_k ********************************/
static inline long int i_trahrhe_k(long int pc, long int i,long int j, long int N) {
 
  if ( ((i>=0) && ((N>=j+1)) && ((j>=0))) && (i_PCMin_1(N) <= pc) && (pc <= i_PCMax_1(N)) ) {
 
  long int k = floorl(creall(((long double)2*(long double)pc+(long double)2*(long double)i*(long double)j-(long double)2*(long double)N*(long double)j+(long double)N*cpowl((long double)i,(long double)2)-(long double)2*cpowl((long double)N,(long double)2)*(long double)i-(long double)N*(long double)i+(long double)2*(long double)i-(long double)2)/(long double)2)+0.0000001);
  if ((i>=0) && (((0<=j) && (j<N))) && (((i<=k) && (k<N)))) {
    return k;
  }
  }
 
  fprintf(stderr,"Error i_trahrhe_k: no corresponding domain: (pc, i,j, N) = (%ld,%ld, %ld, %ld)\n",pc,i,j,N);
  exit(1);
} /* end i_trahrhe_k */
 
/******************************** j Ehrhart Polynomials ********************************/
static inline long int j_Ehrhart(long int N, long int lbi, long int ubi) {
 
  if ((N>0) && ((lbi>=0)) && (((lbi<=ubi) && (ubi<N)))) {
    return floorl(creall(-((long double)N*((long double)ubi-(long double)lbi+(long double)1)*((long double)ubi+(long double)lbi-(long double)2*(long double)N))/(long double)2)+0.0000001);
  }
  fprintf(stderr,"Error j_Ehrhart: no corresponding domain: (N, lbi, ubi) = (%ld, %ld, %ld)\n",N, lbi, ubi);
  exit(1);
}  /* end j_Ehrhart */
 
/******************************** j Ranking Polynomials ********************************/
static inline long int j_Ranking(long int j, long int i, long int k,long int N, long int lbi, long int ubi) {
 
  if ((lbi>=0) && ((ubi<N)) && (((0<=j) && (j<N))) && (((lbi<=i) && (i<=ubi))) && (((i<=k) && (k<N)))) {
    return floorl(creall(((long double)ubi*(((long double)2*(long double)N-(long double)1)*(long double)j-(long double)j*(long double)ubi)+(long double)lbi*(((long double)j+(long double)1)*(long double)lbi+((-(long double)2*(long double)N)-(long double)1)*(long double)j-(long double)2*(long double)N-(long double)1)+(long double)2*(long double)k+(long double)2*(long double)N*(long double)j+((-(long double)i)+(long double)2*(long double)N-(long double)1)*(long double)i+(long double)2)/(long double)2)+0.0000001);
  }
  fprintf(stderr,"Error j_Ranking: no corresponding domain: (j, i, k, N, lbi, ubi) = (%ld, %ld, %ld, %ld, %ld, %ld)\n",j, i, k,N, lbi, ubi);
  exit(1);
} /* end j_Ranking */
 
/******************************** j_PCMin ********************************/
/******************************** j_PCMin_1 ********************************/
static inline long int j_PCMin_1(long int N, long int lbi, long int ubi) {
 
  if ((lbi>=0) && ((N>=ubi+1)) && ((ubi>=lbi))) {
    return floorl(creall((long double)1)+0.0000001);
  }
  return j_Ehrhart(N, lbi, ubi);
} /* end j_PCMin_1 */
 
/******************************** j_PCMax ********************************/
/******************************** j_PCMax_1 ********************************/
static inline long int j_PCMax_1(long int N, long int lbi, long int ubi) {
 
  if ((lbi>=0) && ((N>=ubi+1)) && ((ubi>=lbi))) {
    return floorl(creall(-((long double)N*((long double)ubi-(long double)lbi+(long double)1)*((long double)ubi+(long double)lbi-(long double)2*(long double)N))/(long double)2)+0.0000001);
  }
  return 0;
} /* end j_PCMax_1 */
 
/******************************** j_trahrhe_j ********************************/
static inline long int j_trahrhe_j(long int pc, long int N, long int lbi, long int ubi) {
 
  if ( ((lbi>=0) && ((N>=ubi+1)) && ((ubi>=lbi))) && (j_PCMin_1(N, lbi, ubi) <= pc) && (pc <= j_PCMax_1(N, lbi, ubi)) ) {
 
  long int j = floorl(creall(-((long double)2*((long double)pc-(long double)1))/(((long double)ubi-(long double)lbi+(long double)1)*((long double)ubi+(long double)lbi-(long double)2*(long double)N)))+0.0000001);
  if ((lbi>=0) && ((N>=ubi+1)) && ((ubi>=lbi)) && ((N>=j+1)) && ((j>=0))) {
    return j;
  }
  }
 
  fprintf(stderr,"Error j_trahrhe_j: no corresponding domain: (pc, N, lbi, ubi) = (%ld,%ld, %ld, %ld)\n",pc,N, lbi, ubi);
  exit(1);
} /* end j_trahrhe_j */
 
/******************************** j_trahrhe_i ********************************/
static inline long int j_trahrhe_i(long int pc, long int j, long int N, long int lbi, long int ubi) {
 
  if ( ((lbi>=0) && ((N>=ubi+1)) && ((N>=j+1)) && ((j>=0))) && (j_PCMin_1(N, lbi, ubi) <= pc) && (pc <= j_PCMax_1(N, lbi, ubi)) ) {
 
  long int i = floorl(creall(-(csqrtl((-(long double)4*(long double)j*cpowl((long double)ubi,(long double)2))+((long double)8*(long double)N-(long double)4)*(long double)j*(long double)ubi-(long double)8*(long double)pc+((long double)4*(long double)j+(long double)4)*cpowl((long double)lbi,(long double)2)+(((-(long double)8*(long double)N)-(long double)4)*(long double)j-(long double)8*(long double)N-(long double)4)*(long double)lbi+(long double)8*(long double)N*(long double)j+(long double)4*cpowl((long double)N,(long double)2)+(long double)4*(long double)N+(long double)9)-(long double)2*(long double)N-(long double)1)/(long double)2)+0.0000001);
  if ((lbi>=0) && ((N>=ubi+1)) && ((N>=j+1)) && ((j>=0)) && ((i>=lbi)) && ((ubi>=i))) {
    return i;
  }
  }
 
  fprintf(stderr,"Error j_trahrhe_i: no corresponding domain: (pc, j, N, lbi, ubi) = (%ld,%ld, %ld, %ld, %ld)\n",pc,j,N, lbi, ubi);
  exit(1);
} /* end j_trahrhe_i */
 
/******************************** j_trahrhe_k ********************************/
static inline long int j_trahrhe_k(long int pc, long int j,long int i, long int N, long int lbi, long int ubi) {
 
  if ( ((lbi>=0) && ((N>=ubi+1)) && ((N>=j+1)) && ((j>=0)) && ((i>=lbi)) && ((ubi>=i))) && (j_PCMin_1(N, lbi, ubi) <= pc) && (pc <= j_PCMax_1(N, lbi, ubi)) ) {
 
  long int k = floorl(creall(((long double)ubi*((long double)j*(long double)ubi+((long double)1-(long double)2*(long double)N)*(long double)j)+(long double)2*(long double)pc+(long double)lbi*(((-(long double)j)-(long double)1)*(long double)lbi+((long double)2*(long double)N+(long double)1)*(long double)j+(long double)2*(long double)N+(long double)1)-(long double)2*(long double)N*(long double)j+(long double)i*((long double)i-(long double)2*(long double)N+(long double)1)-(long double)2)/(long double)2)+0.0000001);
  if ((lbi>=0) && ((ubi<N)) && (((0<=j) && (j<N))) && (((lbi<=i) && (i<=ubi))) && (((i<=k) && (k<N)))) {
    return k;
  }
  }
 
  fprintf(stderr,"Error j_trahrhe_k: no corresponding domain: (pc, j,i, N, lbi, ubi) = (%ld,%ld, %ld, %ld, %ld, %ld)\n",pc,j,i,N, lbi, ubi);
  exit(1);
} /* end j_trahrhe_k */
 
