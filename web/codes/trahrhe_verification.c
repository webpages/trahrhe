#include <stdio.h>
#include <math.h>
#include "trahrhe_header.h"
 
#define ceild(n,d)  ceil(((double)(n))/((double)(d)))
#define floord(n,d) floor(((double)(n))/((double)(d)))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))
 
int main(int argc, char *argv[]) {
 
  FILE* fp;
  long int i, j, k;
  long int N;
  long int trahrhe, pc=1;
  long int ltc;
  long int nberrors=0;
 
  if (argc < 2) {
    printf("Usage: %s N\n",argv[0]);
    return 1;
  }
 
  N = atoi(argv[1]);
 
  fp=fopen("trahrhe_ERRORS.txt","w");
  setbuf(stdout, NULL);
  putchar('[');
  for (int i = 0;  i < 100;  i++) putchar('.');
  putchar(']');
  putchar('\r');
  putchar('[');
 
  ltc=(Ehrhart(N)/100);
 
for (i = 0; i < N; i += 1)
  for (j = 0; j < N; j += 1)
    for (k = i; k < N; k += 1) {
        trahrhe = trahrhe_i(pc,N);
        if (trahrhe != i) {
          fprintf(fp,"ERROR: i = %ld <-> trahrhe_i(%ld,%ld) = %ld\n",i,pc,N,trahrhe);
          nberrors++;
        }
        trahrhe = trahrhe_j(pc,i,N);
        if (trahrhe != j) {
          fprintf(fp,"ERROR: j = %ld <-> trahrhe_j(%ld,%ld,%ld) = %ld\n",j,pc,i,N,trahrhe);
          nberrors++;
        }
        trahrhe = trahrhe_k(pc,i,j,N);
        if (trahrhe != k) {
          fprintf(fp,"ERROR: k = %ld <-> trahrhe_k(%ld,%ld,%ld,%ld) = %ld\n",k,pc,i,j,N,trahrhe);
          nberrors++;
        }
        if (pc%max(ltc,1) == 0) putchar('#');
        pc++;
      }
  putchar('\n');
  printf("Verification complete: ");
  if (nberrors == 0) { printf("no errors detected, congrats!\n"); remove("trahrhe_ERRORS.txt"); }
  else printf("%ld errors detected and reported in file trahrhe_ERRORS.txt\n",nberrors);
  fclose(fp);
  return 0;
}
