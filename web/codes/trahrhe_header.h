#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
 
/******************************** Ehrhart Polynomials ********************************/
static inline long int Ehrhart(long int N) {
 
  if ((N>0)) {
    return floorl(creall((cpowl((long double)N,(long double)3)+cpowl((long double)N,(long double)2))/(long double)2)+0.0000001);
  }
  fprintf(stderr,"Error Ehrhart: no corresponding domain: (N) = (%ld)\n",N);
  exit(1);
}  /* end Ehrhart */
 
/******************************** Ranking Polynomials ********************************/
static inline long int Ranking(long int i, long int j, long int k,long int N) {
 
  if ((i>=0) && (((0<=j) && (j<N))) && (((i<=k) && (k<N)))) {
    return floorl(creall(((long double)2*(long double)k+((long double)2*(long double)N-(long double)2*(long double)i)*(long double)j-(long double)N*cpowl((long double)i,(long double)2)+((long double)2*cpowl((long double)N,(long double)2)+(long double)N-(long double)2)*(long double)i+(long double)2)/(long double)2)+0.0000001);
  }
  fprintf(stderr,"Error Ranking: no corresponding domain: (i, j, k, N) = (%ld, %ld, %ld, %ld)\n",i, j, k,N);
  exit(1);
} /* end Ranking */
 
/******************************** PCMin ********************************/
/******************************** PCMin_1 ********************************/
static inline long int PCMin_1(long int N) {
 
  if ((N>=1)) {
    return floorl(creall((long double)1)+0.0000001);
  }
  return Ehrhart(N);
} /* end PCMin_1 */
 
/******************************** PCMax ********************************/
/******************************** PCMax_1 ********************************/
static inline long int PCMax_1(long int N) {
 
  if ((N>=1)) {
    return floorl(creall((cpowl((long double)N,(long double)3)+cpowl((long double)N,(long double)2))/(long double)2)+0.0000001);
  }
  return 0;
} /* end PCMax_1 */
 
/******************************** trahrhe_i ********************************/
static inline long int trahrhe_i(long int pc, long int N) {
 
  if ( (PCMin_1(N) <= pc) && (pc <= PCMax_1(N)) ) {
 
  long int i = floorl(creall(-(csqrtl(-(long double)N*((long double)8*(long double)pc-(long double)4*cpowl((long double)N,(long double)3)-(long double)4*cpowl((long double)N,(long double)2)-(long double)N-(long double)8))-(long double)2*cpowl((long double)N,(long double)2)-(long double)N)/((long double)2*(long double)N))+0.0000001);
  if ((N>=i+1) && ((i>=0))) {
    return i;
  }
  }
 
  fprintf(stderr,"Error trahrhe_i: no corresponding domain: (pc, N) = (%ld,%ld)\n",pc,N);
  exit(1);
} /* end trahrhe_i */
 
/******************************** trahrhe_j ********************************/
static inline long int trahrhe_j(long int pc, long int i, long int N) {
 
  if ( ((N>=i+1) && ((i>=0))) && (PCMin_1(N) <= pc) && (pc <= PCMax_1(N)) ) {
 
  long int j = floorl(creall(-((long double)2*(long double)pc+(long double)N*cpowl((long double)i,(long double)2)-(long double)2*cpowl((long double)N,(long double)2)*(long double)i-(long double)N*(long double)i-(long double)2)/((long double)2*((long double)i-(long double)N)))+0.0000001);
  if ((N>=i+1) && ((i>=0)) && ((N>=j+1)) && ((j>=0))) {
    return j;
  }
  }
 
  fprintf(stderr,"Error trahrhe_j: no corresponding domain: (pc, i, N) = (%ld,%ld, %ld)\n",pc,i,N);
  exit(1);
} /* end trahrhe_j */
 
/******************************** trahrhe_k ********************************/
static inline long int trahrhe_k(long int pc, long int i,long int j, long int N) {
 
  if ( ((i>=0) && ((N>=j+1)) && ((j>=0))) && (PCMin_1(N) <= pc) && (pc <= PCMax_1(N)) ) {
 
  long int k = floorl(creall(((long double)2*(long double)pc+(long double)2*(long double)i*(long double)j-(long double)2*(long double)N*(long double)j+(long double)N*cpowl((long double)i,(long double)2)-(long double)2*cpowl((long double)N,(long double)2)*(long double)i-(long double)N*(long double)i+(long double)2*(long double)i-(long double)2)/(long double)2)+0.0000001);
  if ((i>=0) && (((0<=j) && (j<N))) && (((i<=k) && (k<N)))) {
    return k;
  }
  }
 
  fprintf(stderr,"Error trahrhe_k: no corresponding domain: (pc, i,j, N) = (%ld,%ld, %ld, %ld)\n",pc,i,j,N);
  exit(1);
} /* end trahrhe_k */
 
