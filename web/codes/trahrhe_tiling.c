#include <stdio.h>
#include <math.h>
#include "trahrhe_tiling_header.h"
 
#define ceild(n,d)  ceil(((double)(n))/((double)(d)))
#define floord(n,d) floor(((double)(n))/((double)(d)))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))
 
int main(int argc, char *argv[]) {
 
  FILE* fp;
  long int i, j, k;
  long int N;
  long int trahrhe, pc=1;
  long int ltc;
  long int nberrors=0;
  long int deviation[4]={0, 0, 0, 0}, dcpt[4]={0, 0, 0, 0};
 
  long int Ti, ubTi, lbi, ubi, TILE_VOL_L1, i_pcmax;
  long int Tj, ubTj, lbj, ubj, TILE_VOL_L2, j_pcmax;
 
  if (argc < 4) {
    printf("Usage: %s N TILE_VOL_L1 TILE_VOL_L2\n",argv[0]);
    return 1;
  }
 
  N = atoi(argv[1]);
  TILE_VOL_L1 = atoi(argv[2]);
  TILE_VOL_L2 = atoi(argv[3]);
 
  if (TILE_VOL_L1 < TILEMIN(N)) {
    printf("TILE_VOL_L1 must be greater than %ld (greater than N^2)\n",TILEMIN(N));
    return 1;
  }
 
  printf("Total Number of Iterations: %ld * Minimal Tile Volume: TILE_VOL_L1 = %ld\n",i_Ehrhart(N),TILEMIN(N));
 
  fp=fopen("trahrhe_tiling_ERRORS.txt","w");
  setbuf(stdout, NULL);
  putchar('[');
  for (int _i = 0;  _i < 100;  _i++) putchar('.');
  putchar(']');
  putchar('\r');
  putchar('[');
 
  ltc=(i_Ehrhart(N)/100);
 
 i_pcmax = i_Ehrhart(N);
 ubTi = i_pcmax/(TILE_VOL_L1+1);
 for (Ti = 0; Ti <= ubTi; Ti++) {
  lbi = i_trahrhe_i(max(Ti*(TILE_VOL_L1+1),1),N);
  ubi = i_trahrhe_i(min((Ti+1)*(TILE_VOL_L1+1),i_pcmax),N) - 1;
  if (Ti == ubTi) ubi = N-1;
  
  if (ubi < lbi) { fprintf(stderr, "\nThe tile volume of level 1 (%ld) seems too small, raise it and try again\n",TILE_VOL_L1); exit(1); }
  
 j_pcmax = j_Ehrhart(N, lbi, ubi);
 
 deviation[1]+=abs(j_pcmax - TILE_VOL_L1);
 dcpt[1]++;
 
 ubTj = j_pcmax/(TILE_VOL_L2+1);
 for (Tj = 0; Tj <= ubTj; Tj++) {
  lbj = j_trahrhe_j(max(Tj*(TILE_VOL_L2+1),1),N, lbi, ubi);
  ubj = j_trahrhe_j(min((Tj+1)*(TILE_VOL_L2+1),j_pcmax),N, lbi, ubi) - 1;
  if (Tj == ubTj) ubj = N-1;
  
  if (ubj < lbj) { fprintf(stderr, "\nThe tile volume of level 2 (%ld) seems too small, raise it and try again\n",TILE_VOL_L2); exit(1); }
  
 
 deviation[2]+=abs(j_Ehrhart(N, lbi, ubi) - TILE_VOL_L2);
 dcpt[2]++;
 
for (i = max(0,lbi); i < min(N,ubi+1); i += 1)
  for (j = max(0,lbj); j < min(N,ubj+1); j += 1)
    for (k = i; k < N; k += 1) {
        trahrhe = i_trahrhe_i(i_Ranking(i, j, k,N),N);
        if (trahrhe != i) {
          fprintf(fp,"ERROR: i = %ld <-> i_trahrhe_i(%ld,%ld) = %ld\n",i,i_Ranking(i, j, k,N),N,trahrhe);
          nberrors++;
        }
        trahrhe = i_trahrhe_j(i_Ranking(i, j, k,N),i,N);
        if (trahrhe != j) {
          fprintf(fp,"ERROR: j = %ld <-> i_trahrhe_j(%ld,%ld,%ld) = %ld\n",j,i_Ranking(i, j, k,N),i,N,trahrhe);
          nberrors++;
        }
        trahrhe = i_trahrhe_k(i_Ranking(i, j, k,N),i,j,N);
        if (trahrhe != k) {
          fprintf(fp,"ERROR: k = %ld <-> i_trahrhe_k(%ld,%ld,%ld,%ld) = %ld\n",k,i_Ranking(i, j, k,N),i,j,N,trahrhe);
          nberrors++;
        }
        if (pc%max(ltc,1) == 0) putchar('#');
        pc++;
      }
} /* end for Tj */
} /* end for Ti */
  putchar('\n');
 
  if (pc-1 != i_pcmax) { fprintf(fp,"ERROR: actual iteration count (%ld) different from what expected (%ld)\n",pc-1,i_pcmax); nberrors++; }
 
  printf("Verification complete: ");
  if (nberrors == 0) { 
    printf("no errors detected, congrats!\n");
    remove("trahrhe_tiling_ERRORS.txt");
    printf("Average absolute deviations: [ TILE_VOL_L1 (%ld): %.0Lf (%.2Lf%%) ] [ TILE_VOL_L2 (%ld): %.0Lf (%.2Lf%%) ] \n",TILE_VOL_L1,(long double)deviation[1]/dcpt[1], ((long double)deviation[1]/dcpt[1])*100/TILE_VOL_L1,TILE_VOL_L2,(long double)deviation[2]/dcpt[2], ((long double)deviation[2]/dcpt[2])*100/TILE_VOL_L2);
  }
  else printf("%ld error(s) detected and reported in file trahrhe_tiling_ERRORS.txt\n",nberrors);
  fclose(fp);
  return 0;
}
